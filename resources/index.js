const { Pool } = require('pg')
// Implicitly uses environment variables
// Ability to make use of a local or remote resource such as a database as shown below.

async function getDatabaseTime() {
    try {
        const pool = new Pool()
        const res = await pool.query('SELECT NOW()')
        console.log(`The database time is ${res.rows[0].now} for the ${process.env.NODE_ENV} environment. The postgres database is ${process.env.PGDATABASE} and the postgres host is ${process.env.PGHOST}.`);
        await pool.end()
    }
    catch (e) {
        console.error('Postgres Error Occurred', e);
        throw e;
    }
}
getDatabaseTime()