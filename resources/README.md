


create a postgres database and create the databases and roles as described here

psql postgres

CREATE ROLE factors WITH LOGIN PASSWORD 'fbwb2_w22cp1'

ALTER ROLE factors CREATEDB;

quit

psql postgres -U factors

CREATE DATABASE factors_production_db;

CREATE DATABASE factors_development_db;

GRANT ALL PRIVILEGES ON DATABASE factors_production_db TO factors;

GRANT ALL PRIVILEGES ON DATABASE factors_development_db TO factors;


npm install


PGUSER=factors \
NODE_ENV=production \
PGHOST=localhost \
PGPASSWORD=fbwb2_w22cp1 \
PGDATABASE=factors_production_db\
PGPORT=5432 \
node index.js


change the variables to reflect development and see the result
