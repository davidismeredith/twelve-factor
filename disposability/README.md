## Twelve Factors - disposability

## Twelve Factors - Build, release, run

docker build -t hartree/disposability:1.0 .

docker-compose up

### here we can see that if the application exits then another is started to replace it.

