require("dotenv").config();
import { LOGGER } from "./utility/logger";
import { app, httpServer } from "./app";
import { environmentConfig } from "./utility/config";

const port = app.get("port");
const env = app.get("env");

/**
 * Start Express server.
 */
const server = httpServer.listen(port, () => {

  LOGGER.info(`GraphQL Server is running at ${environmentConfig.hostName}:${environmentConfig.listenPort} in ${env} mode`);

  if (env === "development") {
    LOGGER.info(`Development mode. GraphQL playground is running at ${environmentConfig.hostName}:${environmentConfig.listenPort}/graphql`);
  }

});

export { server };
