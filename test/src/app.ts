import express from "express";
import { LOGGER } from "./utility/logger";
import compression from "compression";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import { createServer } from "http";
import { ApolloServer } from "apollo-server-express";

import { resolvers, typeDefs, TemperatureServiceImpl } from "./graphql/resolvers";
import { db } from "./db";
import { makeExecutableSchema } from "@graphql-tools/schema";
import { environmentConfig } from "./utility/config";

export const app = express();

// set
app.set("port", environmentConfig.listenPort);
app.set("env", environmentConfig.env);

/*
 get
*/
app.get("/healthCheck", (req, res) => {
    res.send("OK");
});

// use
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

const schema = makeExecutableSchema({typeDefs, resolvers});

let apolloServer: ApolloServer;
async function startServer() {
    apolloServer = new ApolloServer({
        schema,
        context: {
            temperatureService: new TemperatureServiceImpl(db),
        },
        rootValue: db
    });
    await apolloServer.start();
    apolloServer.applyMiddleware({ app });
}
startServer().then(() => LOGGER.info(`Apollo Server started ok`)).catch(e => LOGGER.error(`Apollo Server failed to start ${e.toString()}`));

export const httpServer = createServer(app);