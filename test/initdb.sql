CREATE SCHEMA hartree;

CREATE TABLE hartree.temperature (
  temperature numeric not null,
  location text not null,
  recordedat timestamptz not null default now()
);

SELECT create_hypertable('hartree.temperature', 'recordedat');

CREATE VIEW hartree.lastTwentyMinTemperatures AS (
  SELECT time_bucket('5 seconds', recordedat) AS fivesecinterval,
  location,
    MAX(temperature) AS maxtemp
  FROM hartree.temperature
  WHERE recordedat > NOW() - interval '20 minutes'
  GROUP BY fivesecinterval, location
  ORDER BY fivesecinterval ASC
);
