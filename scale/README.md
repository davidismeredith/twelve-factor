## Twelve Factors - scale

Let's take a look at a way to scale using nginx via the famous round-robin mechanism

docker build -t hartree/scale:1.0 .

docker run -d -e "SERVER=mars" --name=mars hartree/scale:1.0
docker run -d -e "SERVER=pluto" --name=pluto hartree/scale:1.0
docker run -d -e "SERVER=saturn" --name=saturn hartree/scale:1.0

docker run -d -p 8080:80 --link saturn --link mars --link pluto hartree/nginx-scale:1.0

call the following command several times

curl localhost:8080
